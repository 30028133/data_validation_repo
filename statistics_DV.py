# function to be used. 

def nan_counter(df):
    '''
    This function takes in a dataframe and returns a new dataframe with the nan values flagged and also mean, median, variance and standard deviation for all columns 
    '''
    # create a new dataframe with the nan values flagged
    df_new = df.copy()
    # create a list of columns
    cols = df_new.columns.tolist()
    # iterate through the columns
    for col in cols:
        # calculate the nan values
        nan_count = df_new[col].isna().sum()
        # calculate the mean, median, variance and standard deviation for the column
        mean = df_new[col].mean()
        median = df_new[col].median()
        variance = df_new[col].var()
        std = df_new[col].std()
        # add the new columns to the dataframe
        df_new[f'{col}_nan_count'] = nan_count
        df_new[f'{col}_mean'] = mean
        df_new[f'{col}_median'] = median
        df_new[f'{col}_variance'] = variance
        df_new[f'{col}_std'] = std
    # return the new dataframe
    return df_new



# Sample data check

import math
import pandas as pd
import numpy as np
c = {
    'a':[np.random.randint(1, 20) for i in range(10)], 
    'b':[i **2 + math.sin(i) for i in range(10)]}

df = pd.DataFrame(c)
df.a[4] = np.nan
df.a[7] = np.nan
df.b[5] = np.nan

__ = nan_counter(df)
print(__)