from plot_graph import plot
from functools import partial
from hyper_param import find_som
from cluster_data import find_km_cluster
from test_som import test_model
from train_som import train_som_cluster
from data_pre_process import data_clean
from gcs_read_write import gcs_bucket_read_directly, gcs_bucket_upload_directly
from sklearn.cluster import KMeans
from google.cloud import storage
import os
import pickle
import datetime
import warnings
import json
import yaml
from datetime import date
import numpy as np
import pandas as pd
import itertools
warnings.filterwarnings("ignore")

# Function to load yaml configuration file
def load_config(config_name):
    ''' loding config file '''
    CONFIG_PATH = "./"
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config


def log_writer(logfile, VAR_CONTENT):
    '''
    Parameters
    ----------
    logfile : TYPE  - log file object
        DESCRIPTION -  file object.
    VAR_CONTENT : string
        DESCRIPTION -  message to be written.
    Returns
    -------
    None.
    '''
    with open(logfile, 'a') as file_handler:
        current_time = datetime.datetime.now()
        file_handler.write(str(current_time) + " " + VAR_CONTENT + "\n")

def main(log_file, data,plant, ins_var, model_metadata):

    global VAR_CONTENT
    source_id_neuron = data['SOURCE_ID_NEURON'][0].split('/')

    PLANT = plant
    ASSET_CATEGORY = ins_var

#  COMMENT STARTS

    if len(data) == 0:
        VAR_CONTENT = "NO RECORDS IN THE INPUT DATASET"
        log_writer(log_file, VAR_CONTENT)
        return 1

    data_set1 = data.copy()

    VAR_CONTENT = "DATA COPIED TO A NEW VARIABLE DATA_SET1"
    log_writer(log_file, VAR_CONTENT)

    pre_processed_df = []
    if ins_var == "TRANSFORMER":
        data_set1["BLOCK"] = data_set1['SOURCE_ID_NEURON'].apply(
            lambda x: x.split("/")[3])

        trafo_data_set = data_set1.groupby(['BLOCK'])
        trafo_block_df = []
        for name, group in trafo_data_set:
            if len(group) != 0:
                block_processed_data = data_clean(group, ins_var)
                if not isinstance(block_processed_data, pd.DataFrame):
                    VAR_CONTENT = "NOT A DATAFRAME"
                    log_writer(log_file, VAR_CONTENT)
                    return 1
                else:
                    trafo_block_df.append(block_processed_data)

        pre_processed_df = pd.concat(trafo_block_df)

    else:
        pre_processed_df = data_clean(data_set1, ins_var)

        if not isinstance(pre_processed_df, pd.DataFrame):
            VAR_CONTENT = "NOT A DATAFRAME"
            log_writer(log_file, VAR_CONTENT)
            return 1

    VAR_CONTENT = "PIVOTING DONE"
    log_writer(log_file, VAR_CONTENT)

    if 'TIMESTAMP' in pre_processed_df.columns:
        pre_processed_df.drop(['TIMESTAMP'], axis=1, inplace=True)

    VAR_CONTENT = "PRE-PROCESSING AFTER PIVOTING FAIL"
    pre_processed_df = pre_processed_df.interpolate(method='time')
    pre_processed_df = pre_processed_df.resample('5min').mean()
    pre_processed_df = pre_processed_df.drop_duplicates()
    pre_processed_df = pre_processed_df.between_time('6:30', '18:30')
    pre_processed_df.dropna(inplace=True)
    VAR_CONTENT = "PRE-PROCESSING AFTER PIVOTING DONE"
    log_writer(log_file, VAR_CONTENT)

    VAR_CONTENT = "FILTERING REQUIRED COLUMNS FAILED"
    if ins_var == "WMS":
        print(pre_processed_df.columns)
        print(config['wms_train_columns'])
        final_df = pre_processed_df.loc[:, config['wms_train_columns']]
        sav_file_name = plant +"_"+ config['wms_sav_file']  + ".sav"
        som_file_name = plant +"_"+ config['wms_npy_file']  + ".npy"
        means_file_name = plant +"_"+ config['wms_means_file']  + ".json"
        
        absmax_file_name = plant +"_"+ config['wms_absmax_file']  +".json"
        model_path = config['gcs_output'] + plant + \
            "/" + config['gcs_models'] + config['gcs_wms']

    elif ins_var == "INVERTER":
        final_df = pre_processed_df.loc[:, config['inverter_train_columns']]
        sav_file_name = plant +"_"+ config['inverter_sav_File']  + ".sav"
        som_file_name = plant +"_"+ config['inverter_npy_File']  + ".npy"
        means_file_name = plant +"_"+ config['inverter_means_file'] +".json"
        absmax_file_name = plant +"_"+ config['inverter_absmax_file']  +".json"
        model_path = config['gcs_output'] + plant + \
            "/" + config['gcs_models'] + config['gcs_inv']
    else:
        final_df = pre_processed_df.loc[:, config['transformer_train_columns']]
        sav_file_name = plant +"_"+ config['trafo_sav_File']  + ".sav"
        som_file_name = plant +"_"+ config['trafo_npy_File']  + ".npy"
        means_file_name = plant +"_"+ config['trafo_means_file']  +".json"
        absmax_file_name = plant +"_"+ config['trafo_absmax_file']  +".json"
        model_path = config['gcs_output'] + plant + \
            "/" + config['gcs_models'] + config['gcs_trafo']
    VAR_CONTENT = "FILTERING REQUIRED COLUMNS SUCCESS"
    log_writer(log_file, VAR_CONTENT)

    VAR_CONTENT = "FILTERING REQUIRED COLUMNS SUCCESS"

    TAGS = [col for col in final_df.columns]

    invalid_data = False
    for col1 in final_df.columns:

        if ins_var == 'TRANSFORMER':

            if col1.endswith('IMPORT_ACTIVE_ENERGY'):
                final_df[col1] = final_df[col1].abs()

            if col1 in ['LHS_LT_PANEL/MFM_LT_PANEL/IMPORT_ACTIVE_ENERGY', 'RHS_LT_PANEL/MFM_LT_PANEL/IMPORT_ACTIVE_ENERGY']:
                final_df.drop([col1], axis=1, inplace=True)
                continue  # it not continued then it will throw error for below codes

        if len(final_df[col1].unique()) == 1:
            if final_df[col1].unique() == 0:
                invalid_data = True
                VAR_CONTENT = "INVALID DATA FOR " + \
                    ins_var + " IN " + str(col1)
                log_writer(log_file, VAR_CONTENT)
            if invalid_data == True:
                return 1

    test_df = final_df.iloc[int(len(final_df) * .7):]

    train_set1 = final_df.iloc[0:int(len(final_df) * .7)]

    train_set1.reset_index('TIMESTAMP', inplace=True)
    train_set1.drop('TIMESTAMP', axis=1, inplace=True)

    for typ in train_set1.dtypes:
        if typ not in ['float64', 'int64']:
            VAR_CONTENT = "Only Numeric values allowed"
            log_writer(log_file, VAR_CONTENT)
            return 1

    for column in train_set1.columns:
        train_set1[column] = train_set1[column] / \
            train_set1[column].abs().max()

    if len(train_set1) == 0:
        VAR_CONTENT = "NO RECORDS AFTER PRE-PROCESSSING "
        log_writer(log_file, VAR_CONTENT)
        return 1
    final_local_path = os.path.join(config["base_model_path"],model_path)
    if not os.path.exists(final_local_path):
        os.makedirs(final_local_path)
        
    km = find_km_cluster(train_set1)
    if km == 1:
        VAR_CONTENT = "ERROR in K-MEANS CLUSTERING "
        log_writer(log_file, VAR_CONTENT)
        return 1
    else:
        gcs_bucket_upload_directly(config['bucket_name'], km, model_path+sav_file_name, "application/python-pickle")
        VAR_CONTENT = "SAVING K-MEANS PICKLE FILE "
        log_writer(log_file, VAR_CONTENT)
        pickle.dump(km, open(final_local_path+"/"+sav_file_name, 'wb'))

    VAR_CONTENT = "FINDING BEST LEARNING RATE FAIL !!!"

    print(train_set1.shape)
    clust = find_som(train_set1, km, test_df, ins_var)

    gcs_bucket_upload_directly(config['bucket_name'], clust, model_path+som_file_name, "application/octet-stream")
    np.save(final_local_path + "/" + som_file_name,np.array(clust))
    VAR_CONTENT = "SAVING SOM NUMPY FILE "
    log_writer(log_file, VAR_CONTENT)

    Final, X_test, missing, rmse,json_means,abs_max_dict = test_model(clust, km, test_df, ins_var)
##--
    gcs_bucket_upload_directly(config['bucket_name'], json.dumps(json_means), model_path+means_file_name, "application/json")
    with open(final_local_path + "/" +means_file_name, 'w') as f:
        json.dump(json_means, f)    
    VAR_CONTENT = "SAVING MEANS JSON FILE "
    log_writer(log_file, VAR_CONTENT)
    
    gcs_bucket_upload_directly(config['bucket_name'], json.dumps(abs_max_dict), model_path+absmax_file_name, "application/json")
    with open(final_local_path + "/" +absmax_file_name, 'w') as f:
        json.dump(abs_max_dict, f)    
    VAR_CONTENT = "SAVING ABS MAX JSON FILE "
    log_writer(log_file, VAR_CONTENT)

    print('Plotting the graphs...')
    plot(Final, X_test, missing, ins_var)
# =========================

 # if model name alread present then only update date modified
 # also if model not present then take last model id append in the end
 ##
    model_names = []
    model_ids = []
    model_id = ''
    if model_metadata:
        model_names = [key['model_name'] for key in model_metadata]
        model_ids = [key['model_id'] for key in model_metadata]
        model_id = max(model_ids) + 1
    else:
        model_id = 1
    metadata_row = {}

    if som_file_name not in model_names:
        metadata_row = {
            "model_name": som_file_name,
            "model_id": model_id,
            "plant": PLANT,
            "asset_category": ASSET_CATEGORY,
            "tags": TAGS,
            "path": model_path,
            "date": date_time
        }
        return metadata_row
    else:
        return 0

if __name__ == "__main__":

    try:
        global date_time
        date_time = str(date.today())
        #    date_time = str(datetime.datetime.now()).replace(":","_").replace(" ","_")
        log_file_name = 'LOG_' + date_time + '.txt'
        log_file = os.path.join(os.path.dirname(__file__), log_file_name)

        cwd = os.getcwd()

        if os.path.exists(log_file):
            os.remove(log_file)
        log_writer(log_file, "LogFile created")

    # Load config file
        # If config fie fails below then this error must be captured.
        VAR_CONTENT = "CONFIG FILE READ FAIL"
        config = load_config("config.yaml")
        VAR_CONTENT = "CONFIG FILE READ SUCCESS"
        log_writer(log_file, VAR_CONTENT)
    
        VAR_CONTENT = "FAIL TO READ FROM GCS, SINCE NO BUCKET NAME MATCHED or NO PATH FOUND."
    
        if config['model_metadata'] in os.listdir(cwd):
            with open(config['model_metadata'], 'r+') as f:
                model_metadata = json.load(f)
        else:
            model_metadata = []
    
        param = list(itertools.product(config['plants'],config['instruments']))
        
        for plant, ins_var in param:
    
            if ins_var == 'WMS':
                prefix = config['gcs_input'] + plant + "/" + config['gcs_wms']
            elif ins_var == "INVERTER":
                prefix = config['gcs_input'] + plant + "/" + config['gcs_inv']
            else:
                prefix = config['gcs_input'] + plant + "/" + config['gcs_trafo']
    
            VAR_CONTENT = "============================ PROCESSING " + \
                str(ins_var) + " FILE"
            log_writer(log_file, VAR_CONTENT)
    
            VAR_CONTENT = "DATA DOWNLOADING FROM GCS BUCKET FAIL"
#            instrument_dataframe = gcs_bucket_read_directly(config['bucket_name'], prefix)
    
            instrument_dataframe = pd.read_csv("WMS_ONE_YEAR_DATA.csv",encoding='utf-8',sep=',',parse_dates=['timestamp'])
    
            instrument_dataframe.columns = [col.upper() for col in instrument_dataframe.columns]
            VAR_CONTENT = "DATA DOWNLOADING FROM GCS BUCKET SUCCESS"
            log_writer(log_file, VAR_CONTENT)
    
            try:
                metadata_row = main(log_file, instrument_dataframe,plant,
                                    ins_var, model_metadata)
                if metadata_row == 1:
                    print("error in training")
                    VAR_CONTENT = " Error in training"
                    log_writer(log_file, VAR_CONTENT)
                elif metadata_row == 0:
                    print("Metadata already updated")
                else:
                    model_metadata.append(metadata_row)
                    print("write to metadata file")
            except Exception as argument:
                print("Error skip training")
                log_writer(log_file, VAR_CONTENT)
        with open(config['model_metadata'], 'w') as f:
            json.dump(model_metadata, f, indent=4)

    except Exception as argument:
        log_writer(log_file,VAR_CONTENT)
