import yaml
import os
from google.cloud import storage
from google.oauth2 import service_account

# Function to load yaml configuration file
def load_config(config_name):
    ''' loding config file '''
    CONFIG_PATH = "./"
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config

config = load_config("config.yaml")

cwd = os.getcwd()
download_path = str(cwd) + '\\'+ config['download_folder']

def list_blobs_with_prefix(bucket_name, day,prefix=None):
    """Lists all the blobs in the bucket that begin with the prefix.
    """              
    credentials = service_account.Credentials.from_service_account_file(
    'noc-prod-service-adani101-7a83b35d9f68.json')   # service key account to access GCS bucket
    
    storage_client = storage.Client(credentials= credentials,project="noc-prod-service-adani101")
    blobs = storage_client.list_blobs(bucket_name, prefix=prefix)
  #  print("blobs ",blobs)
    i = 0
    for blob in blobs:        
        if blob.size != 0:  # do not download empty file
            print("blob")
    
            i = i+1
#            file_name = blob.name.split('/')[-1]
            file_name = "file" 
   #          final_dest = download_path + str(i) +".txt"          
            final_dest = download_path + file_name + str(day)+'_'+ str(i) + ".txt"
            print(final_dest)
            blob.download_to_filename(final_dest)

for i in range(1,9):
    list_blobs_with_prefix('neuron-to-gcs-raw-data',i,'ADANI_SPV_01_03/2022/2/'+str(i)+'/')
    


