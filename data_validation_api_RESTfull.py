# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 18:00:33 2022

@author: 30061160
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Nov 11 18:51:17 2021

@author: 30061160
"""
import datetime
import glob
import os
import pickle

import numpy as np
import pandas as pd
from flask import Flask, jsonify, request
import yaml
from data_pre_process import data_clean
import json


def log_writer(logfile, record_content):
    """ Write log file """
    with open(logfile, 'a', encoding="utf8") as file_handler:
        current_time = datetime.datetime.now()
        file_handler.write(str(current_time) + " " + record_content + "\n")

def find_bmu(self_organizing_maps, x_weight):
    """ Find the best matching unit for the given data point """
    distance_squared = (np.square(self_organizing_maps - x_weight)).sum(axis=2)  # the distance between the data point and the weight
    return np.unravel_index(np.argmin(distance_squared, axis=None), distance_squared.shape)  # the index of the minimum distance

# Function to load yaml configuration file
#We have to include config file into docker too
def load_config(config_name):
    ''' loding config file '''
    CONFIG_PATH = "./"
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config



app = Flask(__name__)
#loading config file
config = load_config("config.yaml")

#Initial check
if not os.path.exists(os.path.dirname(os.path.realpath(__file__))+'/logs'):
    os.makedirs(os.path.dirname(os.path.realpath(__file__))+'/logs')
""" Test API """ 
log_file = (os.path.dirname(os.path.realpath(__file__)) + "/logs/log.txt")  # log file
if os.path.exists(log_file):  # if log file exists, delete it
    os.remove(log_file)  # delete log file

#From model_id we will find the corresponding path which is mapped and saved into another json file during trainning model
with open(config['read_mapping_path'], 'r') as j: #fixed path inside docker only. Every time we train model we will save json file into docker
    read_mapping_json = json.loads(j.read())
    
@app.route("/", methods=['GET'])
def healthcheck():
    """ Health check """
    return "OK"


@app.route('/metadata', methods=['GET'])
def metadata():
    with open(config['read_mapping_path'], 'r') as j: #fixed path inside docker only. Every time we train model we will save json file into docker
        read_mapping_json = json.loads(j.read())
    return jsonify(read_mapping_json)

#Model API   
@app.route('/predict', methods=['POST'])
def prediction():
    log_writer(log_file, "API is running")  # write log
    record_content = "Received a request"
    log_writer(log_file, record_content)
    error_flag = False  # error flag
    
    test_data = request.get_json()  # get the input json

    #Finding the model path for everynew request since it can point to new equipment.
    for dictionaries in read_mapping_json:
        if test_data['model_id']==dictionaries['model_id']: #If the metadata model id matches with the test data model id get the corresponding path
            model_path=dictionaries['path']# model path
    
    try:
        for model_file in os.listdir(model_path):  # check if model exists
            if model_file.endswith('.sav'):
                all_files = glob.glob(model_path + model_file)  # get all files in model path
        record_content = "Loading the KMeans model"
        log_writer(log_file, record_content)
        model_name = max(all_files, key=os.path.getctime)  # get the latest model
    except:
        error_flag = True
        record_content = "Error in loading the model"
        log_writer(log_file, record_content)

    if not error_flag:
        try:
            with open(model_name, "rb") as model_file:
                model = pickle.load(model_file)  # load the model
            for weights_file in os.listdir(model_path):
                if weights_file.endswith('.npy'):  # check if weights exists
                    all_files = glob.glob(model_path + weights_file)  # get all files in model path
            som_weights = max(all_files, key=os.path.getctime)  # get the latest weights
            record_content = "Loading the weights"
            log_writer(log_file, record_content)
            clust = np.load(som_weights)  # load weights
        except:
            error_flag = True
            record_content = "Error in loading the weights"
            log_writer(log_file, record_content)  # log error
    if not error_flag:
        df_col_values=test_data['v'] #This will be dictionary
        df_col_values['ts']=test_data['ts'] #adding timestamp column (if required else we can comment it)
        
        #Saving column name which is having null since we want to send only the corresponding column in output
        null_columns=[] #list of null columns
        for k,v in df_col_values.items():
            if np.nan(df_col_values[k]): #Check if the value is null
                null_columns.append(k)
        
        test_dataframe = pd.DataFrame.from_dict(df_col_values, orient='columns')  # convert to dataframe
        try:
            test_dataframe = data_clean(test_dataframe)  # clean the data
            if isinstance(test_dataframe, pd.DataFrame):
                if 'timestamp' in test_dataframe.columns:
                    test_dataframe.drop('timestamp', axis=1, inplace=True)
                    
        #######Check if this way the data is coming or directly the columns are coming######
                column_list = [col.split('/')[-1] for col in test_dataframe.columns]
                
                #Final columm list will be taken from config file
                if test_data['model_name'].split('_')[-1]=='INVERTER':
                    final_list = config['inverter_test_columns']
                elif test_data['model_name'].split('_')[-1]=='WMS':
                    final_list = config['wms_test_columns']
                else:
                    final_list = config['transformer_test_columns']

                if len(column_list) != len(final_list):
                    error_flag = True
                    record_content = "Error in the number of columns"
                    log_writer(log_file, record_content)
                else:
                    for col1 in column_list:
                        if col1 not in final_list:  # check if the columns are correct
                            error_flag = True
                            record_content = "Error in the columns"
                            log_writer(log_file, record_content)
            else:
                error_flag = True
                record_content = "Error in the preprocessing the dataframe, not a dataframe"
                log_writer(log_file, record_content)
        except:
            error_flag = True
            record_content = "Error in the reading the file"
            log_writer(log_file, record_content)
    if not error_flag:
        try:
            highlight_missing = ~np.isfinite(test_dataframe)  # find missing values
            mean_of_data = np.nanmean(test_dataframe, 0, keepdims=1)  # get the mean of data
            modified_test_data = np.where(highlight_missing, mean_of_data, test_dataframe)  # replace missing values with mean
            label = model.predict(modified_test_data)  # predict the label
            for i in enumerate(label):
                cluster_labels = label[i]  # get the cluster labels
                coordinates = find_bmu(clust[cluster_labels],
                                modified_test_data[i].reshape(1, test_dataframe.shape[1]))  # find the bmu
                coordinate_a = coordinates[0]  # coordinates of the first cluster
                coordinate_b = coordinates[1]  # coordinates of the second cluster
                for j in range(test_dataframe.shape[1]):  # get the coordinates
                    if highlight_missing.values[i][j] is True:  # if missing value
                        vector = clust[cluster_labels][coordinate_a][coordinate_b]  # get the vector
                        modified_test_data[i][j] = vector[j]  # replace the missing value with the vector
            final_dataframe = pd.DataFrame(modified_test_data, columns=test_dataframe.columns)
            for column in final_dataframe.columns:
                final_dataframe[column] = final_dataframe[column] * test_dataframe[column].abs().max()  # normalize
            
            final_json=test_data.pop('v',None) #This will remove v key value pair from the input json
            #We need to update new v key into final json
            #Since we want the output in an expected format i.e value of new key 'pv' will be dictionary (refer format by atul)
            final_json['pv']={}
            for col in null_columns:
                #This will only return those columns which originally has/have null value(s) and uppend imputed values corresponding to those columns
                final_json['pv'][col]=final_dataframe[col]
                 
            result = jsonify(final_json)  # convert to json
            return result
        except:
            error_flag = True
    if error_flag:
        return jsonify(test_dataframe.to_json())
    return 


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8300, debug=True)
