# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 11:24:10 2021

@author: 30061160
"""


from datetime import datetime
import os
import pandas as pd
#Same function will be written with name model_selection
date=datetime.now()

eqp_name='inverter'
input_file_basepath='D:\\OneDrive - Adani\\Testing code env\\Data_model_pkl_files\\'
model_save_path='D:\\OneDrive - Adani\\Testing code env\\Data_model_pkl_files\\model\\'

def model_utility(eqp_name,input_file_basepath,update_model=False):
    if update_model:
        #This time the read path will be different. It will be imputed file path. 
        read_path=''
        
    else:    
        read_path=input_file_basepath+str(date.year)+'\\'+str(date.month)+'\\'+str(date.day)
    print(read_path)
    
    df= pd.read_json(input_file_basepath+'WMS\\bq-results-20211204-131202-whdcn1yufrlt(1).json',lines=True)
    #df=test_df
    column_string=df.source_id_neuron.unique()[0]
    folders_list=column_string.split('/')
    
    site_name=model_save_path+eqp_name+'\\'+folders_list[1]
    
    if not os.path.exists(site_name):
        os.makedirs(site_name)
        
    if eqp_name=='WMS':
        model_path=site_name+'\\'

    else:
        blk_name=site_name+'\\'+folders_list[3]
        if not os.path.exists(blk_name):
            os.makedirs(blk_name)

        if eqp_name=='inverter':
            ACB_name=blk_name+'\\'+folders_list[4]
            if not os.path.exists(ACB_name):
                os.makedirs(ACB_name)
            model_path=ACB_name+'\\'
        elif eqp_name=='transformer':
            model_path=blk_name+'\\'
        
    return model_path

model_path=model_utility(eqp_name,input_file_basepath)
print(model_path)