# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 11:24:10 2021

@author: 30061160
"""

from datetime import datetime
import os
import pandas as pd
import yaml
#Same function will be written with name model_selection
date=datetime.now()

model_save_path='output\\'

# Function to load yaml configuration file

def load_config(config_name):
    ''' loding config file '''
    CONFIG_PATH = "./"
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config

def model_utility(eqp_name,source_string=None,update_model=False,):
    '''
    eqp_name = [INVERETR, TRANSFORMER,WMS]
    input_file_basepath = 
    source_string = SOURCE_ID_NEURON string
    update_model = TRUe for training model , False for real time prediction 
    '''

    folders_list=source_string.split('/')

    site_name= model_save_path  +folders_list[0] 
    
    if not os.path.exists(site_name):
         os.makedirs(site_name)
        
    if eqp_name=='WMS':  
          model_path= site_name + '\\' + eqp_name +'\\'
          print(model_path)
    else:
        blk_name = site_name + '\\' + eqp_name +'\\' +folders_list[2]       
        if not os.path.exists(blk_name):
            os.makedirs(blk_name)

        if eqp_name=='INVERTER':
            ACB_name = blk_name +'\\'+folders_list[3]
            if not os.path.exists(ACB_name):
                os.makedirs(ACB_name)
            model_path=ACB_name+'\\'
        elif eqp_name=='TRANSFORMER':
            model_path=blk_name+'\\'
        
    return model_path

# read config
config = load_config("config.yaml")
