# -*- coding: utf-8 -*-
"""
Created on Tue Feb  1 09:54:50 2022

@author: Adani ML Team
"""
import yaml
import os
import io
import pickle
import pandas as pd
import numpy as np
from google.cloud import storage
from google.oauth2 import service_account
from io import StringIO,BytesIO

# Function to load yaml configuration file
def load_config(config_name):
    ''' loding config file '''
    CONFIG_PATH = "./"
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config

config = load_config("config.yaml")

def gcs_bucker_read_directly(bucket_name,prefix=None):
    """Lists all the blobs in the bucket that begin with the prefix.
    """              
    credentials = service_account.Credentials.from_service_account_file(
    'dev-noc-ml-adani101-3de4a67128cc.json')   # service key account to access GCS bucket
    
    storage_client = storage.Client(credentials= credentials,project=config['project_id'])

    bucket    = storage_client.get_bucket(bucket_name)
    file_name = list(bucket.list_blobs(prefix=prefix))
    
    for file in file_name:
        blob = bucket.blob(file.name)
        data = blob.download_as_string()
        df   = pd.read_csv(BytesIO(data),encoding='utf-8',sep=',',parse_dates=['timestamp'])
        return df

def gcs_bucket_upload_directly(bucket_name,data,file_name,data_format):
    
    '''
        Uploads a file to the bucket.
    # The ID of your GCS bucket
    # bucket_name = "your-bucket-name"
    # data        =  data to be uploded ex dataframe , json , pickle , numpy file
    # filename    = file name of data along with folder location ex: inputs/ghani/transformer/transformer.csv
    # data_format = "text/csv"                  for dataframe
                    "text/plain"                for text file
                    "application/octet-stream"  for npy file
                    "application/python-pickle" for pickle file
    
    # project_id  = "your-project-id"
    '''
    credentials = service_account.Credentials.from_service_account_file(
    'dev-noc-ml-adani101-3de4a67128cc.json')   # service key account to access GCS bucket
     
    if data_format == "text/csv":
        f = StringIO()
        data.to_csv(f)
        f.seek(0)
    elif data_format == "application/python-pickle":
        f = BytesIO()
        pickle.dump(data,f)
        f.seek(0)
    elif data_format == "application/octet-stream":
        f = BytesIO()
        np.save(f,np.array(data))
        f.seek(0)        
    storage_client = storage.Client(credentials= credentials,project=config['project_id'])
    storage_client.get_bucket(bucket_name).blob(file_name). \
            upload_from_file(f, content_type=data_format)
    
def gcs_bucket_upload_from_location(bucket_name,file_name,upload_path,gcs_bucket_location):
    '''Uploads a file to the bucket.
    # The ID of your GCS bucket
    # bucket_name = "your-bucket-name"
    # The path to your file to upload
    # source_file_name = "local/path/to/file"
    # The ID of your GCS object
    # destination_blob_name = "storage-object-name"  '''
    
    ##Update below code for appending imputed dataframes/data before saving into GCS
    credentials = service_account.Credentials.from_service_account_file(
    'dev-noc-ml-adani101-3de4a67128cc.json')   # service key account to access GCS bucket
    storage_client = storage.Client(credentials= credentials,project=config['project_id'])
    
    bucket = storage_client.bucket(bucket_name)
    destination_blob_name =  gcs_bucket_location + file_name
    blob = bucket.blob(destination_blob_name)   # new file you want to create 
    source_file_name = upload_path + file_name  # file name along with location to upload
    blob.upload_from_filename(source_file_name)

#gcs_bucket_upload_from_local("data-cleaning","pivoted_file.csv")

#gcs_bucket_upload_directly("data-cleaning",data,"input/GHANI/WMS/log.txt","text/csv")

#gcs_bucker_read_directly("data-cleaning",prefix='input/GHANI/WMS')


