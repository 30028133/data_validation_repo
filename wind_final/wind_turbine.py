# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 10:19:21 2022

@author: Adani ML Team
"""

import pandas as pd
import numpy as np  
import os

cwd = os.getcwd()

##############################
# NOTE XXXXX------------------------>Sort based on time
###############################

def format_turbine_raw_data(data_df):
    if 'COMMON' in data_df.columns:
        data_df.drop('COMMON',inplace=True,axis=1)
    columns = data_df.columns
    data_df['COMMON'] = ''
    for col in columns:
        data_df['COMMON'] += data_df[col]
    # delete old columns
    for col in columns:
        data_df.drop(col,inplace=True,axis=1)
        
    data_df['SOURCE_ID_NEURON'] = data_df['COMMON'].apply(lambda x : x.split(":[{")[0])
    data_df['SOURCE_ID_NEURON'] = data_df['SOURCE_ID_NEURON'].str.replace('{/','')
    data_df['VALUE_TIMESTAMP'] = data_df['COMMON'].apply(lambda x : x.split(":[{")[1])
    data_df['VALUE_FLOAT'] = data_df['VALUE_TIMESTAMP'].apply(lambda x : x.split(":")[2])
    data_df['VALUE_FLOAT'] = data_df['VALUE_FLOAT'].str.replace('ts','')
    data_df['VALUE_FLOAT'] = pd.to_numeric(data_df['VALUE_FLOAT'])

    data_df['TIMESTAMP'] = data_df['VALUE_TIMESTAMP'].apply(lambda x : x.split('ts:"')[1])
    data_df['TIMESTAMP'] = data_df['TIMESTAMP'].str.replace('"}]}','')
    #data_df['value'] = pd.to_numeric(data_df['value'])
    data_df.drop({'COMMON','VALUE_TIMESTAMP'},inplace=True,axis=1)
    data_df['PLANT']      = data_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[0])
    data_df['TURBINE']    = data_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[1])
    #data_df['TURBINE']    = data_df['TURBINE'].apply(lambda x : x.strip())
    data_df['TURBINE']    = data_df['TURBINE'].str.strip()
    data_df['COLUMNS']    = data_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[2])
    data_df['COLUMNS']    = data_df['COLUMNS'].str.strip()
    
    turbine_list = ['DYA_025','DYA_026','DYA_027','DYA_032','DYA_039','DYA_040','DYA_044','DYA_045',
                'DYA_046','DYA_054','DYA_055','DYA_058','DYA_060','DYA_061','DYA_062', 'DYA_063',
                 'D_132','D_133','D_473','D_474','D_475', 'D_476', 'D_482','D_483', 'D_754']

    filtered_turbine_rows = data_df['TURBINE'].str.contains('|'.join(turbine_list))
    filtered_turbine_data = data_df[filtered_turbine_rows]
    
    feature_list = ['PlcSysTim','ExTmp','W','RotSpd','Rotspe','RotSpeCh','Spd','WdSpd','WdSPdFil','PtAngValBl1','PtAngValBl2','PtAngValBl3',
    'Dir','WdDir','Dmdw_wt','PowDemRamLim','PowLimInf','Var','DayPowAct','Dmdw_wt','GbxMaxGnSpd','PtTrqBl1','PtTrqBl2','PtTrqBl3',
    'PtAngValBl','AccXDir','AccYDir','WdSpdTrb','TrmTmpGbxOil','IntlTmp','TrmTmpRotBrg','PlcSt']

    filtered_features_rows = filtered_turbine_data['COLUMNS'].str.contains('|'.join(feature_list))
    filtered_features_data = filtered_turbine_data[filtered_features_rows]
    
    print(len(filtered_features_data))
    return filtered_features_data

input_path = os.path.join(cwd,"input","data")

file_list = os.listdir(input_path)
#print(file_list)
concat_data = []
error_file_list   = []
i = 0
old_file = ""
split_file_name = ""
first = True
print("start")
for file in file_list:
         file_name = input_path+"\\"+file
         split_file_name = file.split('_')[0]   # this will extract date from file
                                                # ex : file = file1_100.txt  -->
         
         
         try:
               read_data_df = pd.read_csv(file_name)
               filtered_features_data = format_turbine_raw_data(read_data_df)
              
               if old_file != split_file_name:       # i 
                  if first == True:                  #
                      first = False
                      old_file = split_file_name
                  else:
                      wind_data_df = pd.concat(concat_data)
                      wind_data_df.to_csv("output/"+old_file +"_"+"final_wind_data.csv",index=False)
                      concat_data= []
                      old_file = split_file_name
                 
               if len(filtered_features_data) != 0:
                   concat_data.append(filtered_features_data)
         except :
              i = i + 1
              print(file)
         if i == 50:
             break

print(i)
wind_data_df = pd.concat(concat_data)
wind_data_df.to_csv("output/"+split_file_name+"_"+"final_wind_data.csv",index=False)

# file_list_df = pd.DataFrame(error_file_list)
# file_list_df.to_csv("file_list.csv",index=False)
