# -*- coding: utf-8 -*-
"""
Created on Mon Dec 27 09:44:09 2021

@author: Adani ML Team
"""
import os
import pandas as pd
import yaml

# Function to load yaml configuration file
def load_config(config_name):
    ''' loding config file '''
    CONFIG_PATH = "./"
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config

def combine_dataframe(data_df):

    ''' combine all the columns into 1 single column '''
    if 'COMMON' in data_df.columns:
        data_df.drop('COMMON',inplace=True,axis=1)
    columns = data_df.columns
    data_df['COMMON'] = ''
    for col in columns:
        data_df['COMMON'] += data_df[col]
    # delete old columns
    for col in columns:
        data_df.drop(col,inplace=True,axis=1)

    # Source_id_neuron
    data_df['SOURCE_ID_NEURON'] = data_df['COMMON'].apply(lambda x : x.split(":[{")[0])
    data_df['SOURCE_ID_NEURON'] = data_df['SOURCE_ID_NEURON'].str.replace('{/','')

    ## Code 2 step 1
    data_df['VALUE_TIMESTAMP'] = data_df['COMMON'].apply(lambda x : x.split(":[{")[1])

    ## Code 2 step 2
    data_df['VALUE_FLOAT'] = data_df['VALUE_TIMESTAMP'].apply(lambda x : x.split(":")[2])
    data_df['VALUE_FLOAT'] = data_df['VALUE_FLOAT'].str.replace('ts','')
    data_df['VALUE_FLOAT'] = pd.to_numeric(data_df['VALUE_FLOAT'])

    data_df['TIMESTAMP'] = data_df['VALUE_TIMESTAMP'].apply(lambda x : x.split('ts:"')[1])
    data_df['TIMESTAMP'] = data_df['TIMESTAMP'].str.replace('"}]}','')
    #data_df['value'] = pd.to_numeric(data_df['value'])
    data_df.drop({'COMMON','VALUE_TIMESTAMP'},inplace=True,axis=1)

    return data_df

def save_file(groups,ins):
    ''' save file in different directory based on instrument type '''
    #global variable path

    if ins == 'WMS':
        final_location = os.path.join(cwd,config['save_folder'],ins)
        file_name = config['wms_input_file']
        final_file_name = final_location + "\\" + file_name 

        if not os.path.exists(final_location):
            os.makedirs(final_location)
            groups.to_csv(final_file_name,index=True)
        else:
            if file_name in os.listdir(final_location):
                old_file = pd.read_csv(final_file_name)
                final_df = pd.concat([old_file,groups])
                final_df.drop_duplicates(inplace=True)
                final_df.to_csv(final_file_name,index=False)            

    if ins in ('INVERTER' ,'TRANSFORMER'):

        for name,group in groups:
            file1 = ""
            file_path = ""

            for n_var in name:
                file1 = file1 + n_var + "_"
                file_path = file_path + n_var + '\\'
            final_location = os.path.join(cwd,config['save_folder'],ins,file_path)
            file_name = file1 + ins + ".csv"
            final_file_name = final_location + "\\" + file_name
            if not os.path.exists(final_location):
                os.makedirs(final_location)
                group.to_csv(final_file_name,index=False)
            else:
                if file_name in os.listdir(final_location):
                    old_file = pd.read_csv(final_file_name)
                    final_df = pd.concat([old_file,group])
                    final_df.drop_duplicates(inplace=True)
                    final_df.to_csv(final_file_name,index=False)            
                    
def identify_instrument(common_df):
    
    instrument = ""

    col1 = common_df['SOURCE_ID_NEURON'].str.contains('CTR') & \
                            common_df['SOURCE_ID_NEURON'].str.contains('Y_B_PHASE_VOLTAGE')
    col2 = common_df['SOURCE_ID_NEURON'].str.contains('CTR') & \
                                                common_df['SOURCE_ID_NEURON'].str.contains('R_Y_PHASE_VOLTAGE')
    if col1 | col2 :
        instrument = 'WMS'
    else:
        common_df['PLANT']        = common_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[0])
        common_df['BLOCK_NUMBER'] = common_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[2])
        common_df['ACB_PANEL']    = common_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[3])
        
        if common_df['SOURCE_ID_NEURON'].str.contains('ACB').sum():
            instrument = 'INVERTER'
        else:
            trafo_rows = common_df['SOURCE_ID_NEURON'].str.contains('LHS_LT_PANEL') \
                        | common_df['SOURCE_ID_NEURON'].str.contains('RHS_LT_PANEL') \
                        | common_df['SOURCE_ID_NEURON'].str.contains('VCB_PANEL') \
                        | common_df['SOURCE_ID_NEURON'].str.contains('IDT_TRAFO')
            if trafo_rows.sum():
                instrument = "TRANSFORMER"
            else:
                instrument = 'INVALID'
    return instrument

def create_inv_trafo_wms_files(common_df):
    ''' create different dataframe based on instrument '''

    instrument = ""
    col1 = common_df['SOURCE_ID_NEURON'].str.contains('CTR') & \
                            common_df['SOURCE_ID_NEURON'].str.contains('Y_B_PHASE_VOLTAGE')
    col2 = common_df['SOURCE_ID_NEURON'].str.contains('CTR') & \
                            common_df['SOURCE_ID_NEURON'].str.contains('R_Y_PHASE_VOLTAGE')

      # col1 = common_df['SOURCE_ID_NEURON'].str.contains('CTR') & \
      #                        common_df['SOURCE_ID_NEURON'].str.contains('RADIATION_GHI')
      # col2 = common_df['SOURCE_ID_NEURON'].str.contains('CTR') & \
      #                        common_df['SOURCE_ID_NEURON'].str.contains('RADIATION_GII')
      # col3 = common_df['SOURCE_ID_NEURON'].str.contains('CTR') & \
      #                        common_df['SOURCE_ID_NEURON'].str.contains('EXPORT_ACTIVE_ENERGY')

    wms_cols = col1 | col2 #| col3

    if wms_cols.sum():
        instrument = "WMS"
        wms_df = common_df.loc[wms_cols]
        save_file(wms_df,instrument)

    # create INVERTER files

    common_df['PLANT']        = common_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[0])
    common_df['BLOCK_NUMBER'] = common_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[2])
    common_df['ACB_PANEL']    = common_df['SOURCE_ID_NEURON'].apply(lambda x : x.split("/")[3])

    inv_rows = common_df['SOURCE_ID_NEURON'].str.contains('ACB')

    if inv_rows.sum():
        instrument = "INVERTER"
        inv_df   = common_df.loc[inv_rows]
        inv_groups = inv_df.groupby(['PLANT','BLOCK_NUMBER','ACB_PANEL'])
        save_file(inv_groups,instrument)

    # create TRANSFORMER files
    trafo_rows = common_df['SOURCE_ID_NEURON'].str.contains('LHS_LT_PANEL') \
                | common_df['SOURCE_ID_NEURON'].str.contains('RHS_LT_PANEL') \
                | common_df['SOURCE_ID_NEURON'].str.contains('VCB_PANEL') \
                | common_df['SOURCE_ID_NEURON'].str.contains('IDT_TRAFO')
    if trafo_rows.sum():
        instrument = "TRANSFORMER"
        trafo_df = common_df[trafo_rows]
        trafo_groups = trafo_df.groupby(['PLANT','BLOCK_NUMBER'])
        save_file(trafo_groups,instrument)

config = load_config("config.yaml")
cwd = os.getcwd()


                                         
                                         
                                         
                                 




