# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 08:58:51 2022

@author: Adani ML Team
"""

import pandas as pd
import numpy as np  
import os
from gcs_read_write import gcs_bucket_upload_from_location

cwd = os.getcwd()

file_location = os.path.join(cwd,"output")

file_list = os.listdir(file_location)

for file in file_list:
       gcs_bucket_upload_from_location("data-cleaning",file,file_location+"\\","wind/")
    



