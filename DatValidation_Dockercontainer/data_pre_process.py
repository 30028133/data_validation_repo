# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 13:16:45 2021

@author: 30061160
"""
import pandas as pd
import numpy
def data_clean(df):
    #df["timestamp"] = pd.to_datetime(df["timestamp"], format='%Y-%m-%d %H:%M:%S %Z', errors='coerce')
#    mask = df.timestamp.isnull()
#    if mask.sum():
#        df.loc[mask, "timestamp"] = pd.to_datetime(df[mask]["timestamp"], format='%Y-%m-%d %H:%M:%S.%f %Z',errors='coerce')
#        df['timestamp']=df['ts']
#        df.drop(columns={'ts'},inplace=True)
#    else:
    df["timestamp"] = pd.to_datetime(df["timestamp"])
    print("df.info() ",df.info())
    try:    
        df.drop(columns = {"plant","rnoc_tag_name","value_type","quality"},inplace=True)
    except KeyError:
        print('errro')
#         content = " the columns plant,rnoc_tag_name,value_type,quality are not present in the data"
#         log_writer(approot,content)
    new_df = df.pivot_table(index = "timestamp",columns="source_id_neuron",values = "value_float",dropna=False)
    new_df = new_df.reset_index()
    
#     content = "converted the table to have multiple columns"
#     log_writer(approot,content)
#     if new_df['timestamp'].dtypes=='datetime64[ns, UTC]':
#         new_df.set_index(new_df['timestamp'],inplace=True)
#         new_df = new_df.tz_convert('Asia/Kolkata')
#         #new_df.drop(columns = "timestamp",inplace=True)
    return new_df